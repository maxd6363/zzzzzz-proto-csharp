﻿using Urho;

namespace ZZZZZZ.Desktop
{
    public class Program
    {
        private static GameManager gameManager;
        static void Main(string[] args)
        {
            var options = new ApplicationOptions("Data")
            {
                LimitFps = true,
                
                WindowedMode = true,
                ResizableWindow = true,
                Width = 1440 / 1,
                Height = 1080 / 1,
                TouchEmulation = true
            };


            gameManager = new GameManager(options);
            gameManager.Run();
        }

    }
}
