﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Urho;
using Urho.Urho2D;
using ZZZZZZ.Utils;

namespace ZZZZZZ.Physics
{
    public class ScenePhysics : Scene
    {
        private PhysicsWorld2D PhysicsWorld2D { get; set; }
        private CollisionHandler CollisionHandler { get; set; }

        

        public ScenePhysics() : base()
        {
            CreateComponent<Octree>();
            PhysicsWorld2D = CreateComponent<PhysicsWorld2D>();
            PhysicsWorld2D.Gravity = AppConfig.Current.PhysicsGavity;
            CollisionHandler = CreateComponent<CollisionHandler>();
            CollisionHandler.PhysicsWorld2D = PhysicsWorld2D;
            CreateComponent<DebugRenderer>();
            UpdateEnabled = true;
        }

        public void InvertGravity()
        {
            PhysicsWorld2D.Gravity *= -1;
            CollisionHandler.PhysicsWorld2D = PhysicsWorld2D;
            
        }

    }
}
