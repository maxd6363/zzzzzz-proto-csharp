﻿using System;
using System.Collections.Generic;
using System.Text;
using Urho;

namespace ZZZZZZ.Utils
{
    public class AppConfig
    {
        private static AppConfig _current = null;
        public static AppConfig Current
        {
            get
            {
                if (_current == null)
                    _current = new AppConfig();
                return _current;
            }
        }

        private AppConfig() { }

        public ICollection<string> PostProcessing { get; } = new List<string>()
        {
            
        };

        public float EntitySpawnAnimationTiming { get => 1.0f; }
        
        
        public float CharacterSpeed { get => 0.6f; }


        public int DefaultFPSMargin { get => 20; }
        public int DefaultFPSFontSize { get => 20; }
        public Color DefaultFPSColor { get => Color.White; }

        public bool DrawDebug { get; set; } = false;

        public Vector2 PhysicsGavity { get => -Vector2.UnitY * 20; }



    }
}
