﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZZZZZZ.Utils
{
    public class Assets
    {
        public static class PostProcess
        {
            public const string AutoExposure = "PostProcess/auto_exposure.xml";
            public const string Bloom = "PostProcess/bloom.xml";
            public const string BloomHDR = "PostProcess/bloom_hdr.xml";
            public const string Blur = "PostProcess/blur.xml";
            public const string ColorCorrection = "PostProcess/color_correction.xml";
            public const string FXAA2 = "PostProcess/fxaa2.xml";
            public const string FXAA3 = "PostProcess/fxaa3.xml";
            public const string GammaCorrection = "PostProcess/gamma_correction.xml";
            public const string GreyScale = "PostProcess/grey_scale.xml";
            public const string Tonemap = "PostProcess/tonemap.xml";
        }

        public static class Fonts
        {
            public const string AnonymousPro = "Fonts/anonymous_pro.ttf";
        }

        public static class Levels
        {
            public const string Level1 = "Levels/level1.tmx";
            public const string LevelTest = "Levels/levelTest.tmx";
        }

        public static class Sprites
        {
            public static class Characters
            {
                public const string Character1 = "Sprites/Characters/character_1.png";
                public const string Character2 = "Sprites/Characters/character_2.png";


            }

        }

    }
}
