﻿using System;
using System.Collections.Generic;
using System.Text;
using Urho;
using ZZZZZZ.Entities.Character;

namespace ZZZZZZ.Inputs
{
    public abstract class AbstractInput
    {
        public bool IsConnected { get; protected set; }
        public Movement Movement { get; protected set; }

        protected Action<InputActionArgs> _gravityChanged;


        public event Action<InputActionArgs> GravityChanged
        {
            add => _gravityChanged += value;
            remove => _gravityChanged -= value;
        }


        public AbstractInput()
        {
            IsConnected = false;
            Application.Current.Update += Update;
        }

        protected virtual void Update(UpdateEventArgs obj)
        {

        }
    }
}
