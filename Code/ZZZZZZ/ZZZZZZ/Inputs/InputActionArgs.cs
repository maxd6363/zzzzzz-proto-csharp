﻿using System;
using System.Collections.Generic;
using System.Text;
using ZZZZZZ.Entities.Character;

namespace ZZZZZZ.Inputs
{
    public struct InputActionArgs
    {
        public Movement Movement;

        public InputActionArgs(Movement movement)
        {
            Movement = movement;

        }

        public override string ToString()
        {
            return $"Movement : {Movement}";
        }
    }
}
