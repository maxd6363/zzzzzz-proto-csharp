﻿using System.Diagnostics;
using Urho;
using ZZZZZZ.Entities.Character;

namespace ZZZZZZ.Inputs
{
    public class InputTouch : AbstractInput
    {

        private readonly int middleScreen = GameManager.Current.Graphics.Size.X / 2;

        public InputTouch()
        {
            IsConnected = true;
            Application.Current.Input.TouchBegin += Input_TouchBegin;
            Application.Current.Input.TouchEnd += Input_TouchEnd;

        }


        private void Input_TouchBegin(TouchBeginEventArgs obj)
        {

            if (obj.X > 0 && obj.X < middleScreen / 2)
            {
                Movement = Movement.Left;
            }
            else if (obj.X > middleScreen * 1.5 && obj.X < middleScreen * 2)
            {
                Movement = Movement.Right;
            }
            else if (obj.X > middleScreen / 2 && obj.X < middleScreen * 1.5)
            {
                _gravityChanged?.Invoke(new InputActionArgs(Movement.Gravity));
            }
            else
            {
                Movement = Movement.None;
            }

        }

        private void Input_TouchEnd(TouchEndEventArgs obj)
        {
            Movement = Movement.None;
        }


        public override string ToString()
        {
            return $"{typeof(InputTouch)}";
        }

    }

}

