﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Urho;
using Urho.Urho2D;
using ZZZZZZ.Utils;

namespace ZZZZZZ.Worlds
{
    public class Level : Component
    {

        public TileMap2D TileMap { get; private set; }


        public Level(string level)
        {
            GameManager.Current.Scene.CreateChild("Level").AddComponent(this);

            TileMap = Node.CreateComponent<TileMap2D>();
            TileMap.TmxFile = Application.ResourceCache.GetTmxFile2D(level);


            TileMap.Node.SetPosition2D(new Vector2(-10.5f, -8f));
            TileMap.Node.Scale *= 4.3f;


            Application.Current.Input.KeyDown += Input_KeyDown;
            var info = TileMap.Info;

            GameManager.Current.Camera.Node.Position = new Vector3(info.MapWidth / 2, info.MapHeight / 2, -1f);
            GameManager.Current.Camera.Zoom /= 5;


            GenerateCollision();


        }

        private void GenerateCollision()
        {
            var layer = TileMap.GetLayer(0);
            for (int i = 0; i < layer.Width; i++)
            {
                for (int j = 0; j < layer.Width; j++)
                {
                    var tile = layer.GetTile(i, j);
                    if (tile != null)
                    {
                        Node node = Node.CreateChild("RigidBody");
                        node.CreateComponent<RigidBody2D>();
                        CollisionBox2D box = node.CreateComponent<CollisionBox2D>();
                        
                        box.Size = Vector2.One * 32 / 100;
                        box.Node.SetPosition2D(TileMap.TileIndexToPosition(i, j) + Vector2.One * 16 / 100);
                    }
                }
            }
        }


        private void Input_KeyDown(KeyDownEventArgs obj)
        {
            if (obj.Key == Key.Z)
                TileMap.Node.SetPosition2D(TileMap.Node.Position2D + Vector2.UnitY / 2);
            if (obj.Key == Key.S)
                TileMap.Node.SetPosition2D(TileMap.Node.Position2D - Vector2.UnitY / 2);
            if (obj.Key == Key.Q)
                TileMap.Node.SetPosition2D(TileMap.Node.Position2D - Vector2.UnitX / 2);
            if (obj.Key == Key.D)
                TileMap.Node.SetPosition2D(TileMap.Node.Position2D + Vector2.UnitX / 2);
            if (obj.Key == Key.E)
                TileMap.Node.Scale += Vector3.One / 10;
            if (obj.Key == Key.A)
                TileMap.Node.Scale -= Vector3.One / 10;
            Debug.WriteLine($"{TileMap.Node.Position2D} {TileMap.Node.Scale}");

        }
    }
}
