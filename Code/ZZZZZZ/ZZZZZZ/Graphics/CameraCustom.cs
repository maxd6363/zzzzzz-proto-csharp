﻿using System;
using System.Collections.Generic;
using System.Text;
using Urho;
using Urho.Actions;
using ZZZZZZ.Utils;

namespace ZZZZZZ.Graphics
{
    public class CameraCustom : Camera
    {


        private float Counter;
        private bool IsRumbling;

        public CameraCustom() : base()
        {
            GameManager.Current.Scene.CreateChild().AddComponent(this);

            
            Orthographic = true;

            Application.Current.Update += Update;
            Counter = 1;
            Zoom = 5;
            IsRumbling = false;
        }


        private void Update(UpdateEventArgs obj)
        {

            if (Counter <= 0 && IsRumbling == true) IsRumbling = false;


            if (IsRumbling)
            {
                var _pos = Node.Position;
                var _delta = 0.4f;
                Node.Position = new Vector3(ToolBox.RandomFloat(_pos.X - _delta, _pos.Y + _delta), ToolBox.RandomFloat(_pos.Y - _delta, _pos.Y + _delta), _pos.Z);
                Counter -= obj.TimeStep;
                return;
            }



        }

        public async void Reset()
        {
            await Node.RunActionsAsync(new EaseBackOut(new MoveTo(1.5f, new Vector3(0, 0, -10))));
        }

        public void Rumble()
        {
            IsRumbling = true;
            Counter = 1;

        }
    }
}
