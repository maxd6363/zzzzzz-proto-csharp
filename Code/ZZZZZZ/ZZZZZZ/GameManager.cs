﻿using System;
using Urho;
using ZZZZZZ.Graphics;
using ZZZZZZ.GUI;
using ZZZZZZ.Inputs;
using ZZZZZZ.Objects.Character;
using ZZZZZZ.Physics;
using ZZZZZZ.Player;
using ZZZZZZ.Utils;
using ZZZZZZ.Worlds;

namespace ZZZZZZ
{
    public class GameManager : Application
    {
        public static new GameManager Current { get => Application.Current as GameManager; }

        public ScenePhysics Scene { get; private set; }
        public CameraCustom Camera { get; private set; }
        public Viewport Viewport { get; private set; }
        public GUIFps FPS { get; private set; }


        public GameManager(ApplicationOptions options) : base(options)
        {

        }

        protected override void Setup()
        {
            base.Setup();
            
        }

        protected override void Start()
        {
            base.Start();
            Scene = new ScenePhysics();
            Camera = new CameraCustom();
            Viewport = new ViewportCustom();
            FPS = new GUIFps();


            var playerManager = new PlayerManager(new Character(), new InputTouch());
            var level = new Level(Assets.Levels.LevelTest);

        }
    }
}
