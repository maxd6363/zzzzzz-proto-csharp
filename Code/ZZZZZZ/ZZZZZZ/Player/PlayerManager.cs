﻿using System;
using System.Collections.Generic;
using System.Text;
using Urho;
using ZZZZZZ.Entities.Character;
using ZZZZZZ.Inputs;
using ZZZZZZ.Objects.Character;

namespace ZZZZZZ.Player
{
    public class PlayerManager
    {
        public Character Character { get; private set; }

        public AbstractInput Input { get; private set; }

        public PlayerManager(Character character, AbstractInput input)
        {
            Character = character;
            Input = input;
            Application.Current.Update += Current_Update;
            Input.GravityChanged += Input_GravityChanged;
        }

        private void Input_GravityChanged(InputActionArgs obj)
        {
            Character.Move(Movement.Gravity);
            GameManager.Current.Scene.InvertGravity();
        }

        private void Current_Update(UpdateEventArgs obj)
        {
            Character.Move(Input.Movement);
      
        }
    }
}
