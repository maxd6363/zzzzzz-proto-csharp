﻿using System;
using System.Collections.Generic;
using System.Text;
using ZZZZZZ.Entities;
using Urho.Urho2D;
using Urho;
using ZZZZZZ.Utils;
using ZZZZZZ.Entities.Character;
using System.Diagnostics;

namespace ZZZZZZ.Objects.Character
{
    public class Character : Entity
    {
        private List<Sprite2D> Sprites;
        private float internalTimer;

        public Character()
        {
            Shape = Node.CreateComponent<CollisionBox2D>();
            (Shape as CollisionBox2D).Size = Vector2.One / 8;
            (Shape as CollisionBox2D).Density = 1000;

            Body.UseFixtureMass = false;
            Body.Inertia = 5;
            Body.FixedRotation = true;

            internalTimer = 0f;

            Sprites = new List<Sprite2D>
            {
                Application.ResourceCache.GetSprite2D(Assets.Sprites.Characters.Character1),
                Application.ResourceCache.GetSprite2D(Assets.Sprites.Characters.Character2)
            };
            foreach (var s in Sprites)
            {
                s.Texture.FilterMode = TextureFilterMode.Nearest;
            }

            Sprite.Sprite = Sprites[0];

            
            Node.Scale = 10 * Vector3.One;
            
        }



        public void Move(Movement movement)
        {
            
            switch (movement)
            {
                case Movement.Left:
                    //Node.SetPosition2D(Node.Position2D - Vector2.UnitX * AppConfig.Current.CharacterSpeed);
                    Body.ApplyLinearImpulseToCenter(-Vector2.UnitX * AppConfig.Current.CharacterSpeed, true);
                    
                    Sprite.FlipX = true;
                    break;
                case Movement.Right:
                    //Node.SetPosition2D(Node.Position2D + Vector2.UnitX * AppConfig.Current.CharacterSpeed);
                    Body.ApplyLinearImpulseToCenter(Vector2.UnitX * AppConfig.Current.CharacterSpeed, true);
                    
                    Sprite.FlipX = false;
                    break;
                case Movement.Gravity:
                    Sprite.FlipY = !Sprite.FlipY;
                    break;
            }
        }

        public override void Update(UpdateEventArgs obj)
        {
            internalTimer = (internalTimer + obj.TimeStep) % 1000;
         
            Sprite.Sprite = Sprites[((int)(internalTimer * 2)) % Sprites.Count];
            
            if(Body.LinearVelocity.Length > 10f)
            {
                Debug.WriteLine($"Throttling : {Body.LinearVelocity.Length}");
                Body.SetLinearVelocity(Body.LinearVelocity*0.90f);
            }
        }
    }
}

