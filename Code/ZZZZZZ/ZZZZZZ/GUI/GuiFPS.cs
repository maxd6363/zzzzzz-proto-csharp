﻿using Urho;
using Urho.Gui;
using ZZZZZZ.Utils;

namespace ZZZZZZ.GUI
{
    public class GUIFps : Text
    {



        public GUIFps() : this(UIPosition.TOP_LEFT)
        {
        }

        public GUIFps(UIPosition position)
        {
            Application.Current.Update += Update;

            switch (position)
            {
                case UIPosition.TOP_LEFT:
                    VerticalAlignment = VerticalAlignment.Top;
                    HorizontalAlignment = HorizontalAlignment.Left;
                    break;

                case UIPosition.TOP_CENTER:
                    VerticalAlignment = VerticalAlignment.Top;
                    HorizontalAlignment = HorizontalAlignment.Center;
                    break;

                case UIPosition.TOP_RIGHT:
                    VerticalAlignment = VerticalAlignment.Top;
                    HorizontalAlignment = HorizontalAlignment.Right;
                    break;

                case UIPosition.CENTER_LEFT:
                    VerticalAlignment = VerticalAlignment.Center;
                    HorizontalAlignment = HorizontalAlignment.Left;
                    break;

                case UIPosition.CENTER_CENTER:
                    VerticalAlignment = VerticalAlignment.Center;
                    HorizontalAlignment = HorizontalAlignment.Center;
                    break;

                case UIPosition.CENTER_RIGHT:
                    VerticalAlignment = VerticalAlignment.Center;
                    HorizontalAlignment = HorizontalAlignment.Right;
                    break;

                case UIPosition.BOTTOM_LEFT:
                    VerticalAlignment = VerticalAlignment.Bottom;
                    HorizontalAlignment = HorizontalAlignment.Left;
                    break;

                case UIPosition.BOTTOM_CENTER:
                    VerticalAlignment = VerticalAlignment.Bottom;
                    HorizontalAlignment = HorizontalAlignment.Center;
                    break;

                case UIPosition.BOTTOM_RIGHT:
                    VerticalAlignment = VerticalAlignment.Bottom;
                    HorizontalAlignment = HorizontalAlignment.Right;
                    break;

                default:
                    VerticalAlignment = VerticalAlignment.Top;
                    HorizontalAlignment = HorizontalAlignment.Left;
                    break;
            }


            Position = IntVector2.One * AppConfig.Current.DefaultFPSMargin;

            SetColor(AppConfig.Current.DefaultFPSColor);
            SetFont(Application.Current.ResourceCache.GetFont(Assets.Fonts.AnonymousPro), AppConfig.Current.DefaultFPSFontSize);
            Application.Current.UI.Root.AddChild(this);

        }

        private void Update(UpdateEventArgs obj)
        {
            Value = $"{(int)(1.0 / obj.TimeStep) + 1}";
        }

        public void Toggle()
        {
            if (Opacity == 0)
                Opacity = 1;
            else
                Opacity = 0;
        }

    }
}
